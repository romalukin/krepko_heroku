FROM python:3.7-slim
COPY . .
RUN pip3 install --no-cache-dir -r requirements.txt
CMD ["python3", "./krepko_bot.py"]

