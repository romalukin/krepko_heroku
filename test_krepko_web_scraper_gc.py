import unittest
from krepko_web_scraper import get_catalog

class TestFunc(unittest.TestCase):

    def test_get_catalog(self):
        catalog = get_catalog("https://krepkoshop.com/category/")
        self.assertIn('https://krepkoshop.com/category/sumki/',catalog.values())
        self.assertIn('https://krepkoshop.com/category/zhenskie-sumki/',catalog.values())
        self.assertIn('https://krepkoshop.com/category/kartkholdery/',catalog.values())
        self.assertIn('Мужские сумки',catalog)
        self.assertIn('Женские сумки',catalog)
        self.assertIn('Для документов',catalog)


if __name__ == '__main__':
    unittest.main()