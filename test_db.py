import unittest
import os
import sqlalchemy
import db

class TestDB(unittest.TestCase):
    def setUp(self):
        url = os.getenv("DATABASE_TEST_URL")
        if not url:
            self.skipTest("No database URL set")
        self.engine = sqlalchemy.create_engine(url)
        self.connection = self.engine.connect()
        self.connection.connection.connection.set_isolation_level(0)
        self.connection.execute("CREATE DATABASE testdb")
        self.connection.connection.connection.set_isolation_level(1)

    def tearDown(self):
        self.connection.connection.connection.set_isolation_level(0)
        self.connection.execute("DROP DATABASE testdb")
        self.connection.connection.connection.set_isolation_level(1)
    
    def test_db_insert(self):
        product_list = [{'name': 'Парные кожаные браслеты Twix', 'old_price': 1490, 'sale': 0, 'price': 1490, 'category': 'Подарочные наборы', 'url': 'https://krepkoshop.com/parnye-kozhanye-braslety-twix/'}, 
                        {'name': 'Набор для BBQ Medium Rare', 'old_price': 9199, 'sale': 0, 'price': 9199, 'category': 'Подарочные наборы', 'url': 'https://krepkoshop.com/379/'}, 
                        {'name': 'Набор для барбекю Well Done', 'old_price': 16899, 'sale': 0, 'price': 16899, 'category': 'Подарочные наборы', 'url': 'https://krepkoshop.com/nabor-dlya-barbekyu-well-done/'}, 
                        {'name': 'Набор подставок на обеденный стол KREPKO', 'old_price': 7990, 'sale': 0, 'price': 7990, 'category': 'Подарочные наборы', 'url': 'https://krepkoshop.com/nabor-podstavok-na-obedennyy-stol-krepko/'}, 
                        {'name': 'Настольный набор руководителя Dumas', 'old_price': 25490, 'sale': 0, 'price': 25490, 'category': 'Подарочные наборы', 'url': 'https://krepkoshop.com/nastolnyy-nabor-rukovoditelya-dumas/'}, 
                        {'name': 'Набор для офиса S. P. J. Plankton', 'old_price': 11990, 'sale': 0, 'price': 11990, 'category': 'Подарочные наборы', 'url': 'https://krepkoshop.com/nabor-dlya-ofisa-s-p-j-plankton/'}
                        ]
        for card in product_list:
            db.insert_product(card['name'], card['old_price'], card['sale'], card['price'], card['category'], card['url'])
        self.assertTrue(db.select_product('Парные кожаные браслеты Twix')['status'])
        self.assertTrue(db.select_product('Набор для BBQ Medium Rare')['status'])
        self.assertTrue(db.select_product('Набор для барбекю Well Done')['status'])
        self.assertTrue(db.select_product('Набор подставок на обеденный стол KREPKO')['status'])
        self.assertTrue(db.select_product('Настольный набор руководителя Dumas')['status'])
        self.assertTrue(db.select_product('Набор для офиса S. P. J. Plankton')['status'])
        
    def test_db_delete(self):
        db.delete_products()
        self.assertFalse(db.select_product('Парные кожаные браслеты Twix')['status'])
        self.assertFalse(db.select_product('Набор для BBQ Medium Rare')['status'])
        self.assertFalse(db.select_product('Набор для барбекю Well Done')['status'])
        self.assertFalse(db.select_product('Набор подставок на обеденный стол KREPKO')['status'])
        self.assertFalse(db.select_product('Настольный набор руководителя Dumas')['status'])
        self.assertFalse(db.select_product('Набор для офиса S. P. J. Plankton')['status'])
        
if __name__ == '__main__':
    unittest.main()
    