import unittest
from krepko_web_scraper import get_catalog
from krepko_web_scraper import get_products

class TestFunc(unittest.TestCase):

    def test_get_products(self):
        category_list = []
        catalog = get_catalog("https://krepkoshop.com/category/")
        for category in catalog:
            products = get_products(category, catalog[category])
            category_list.append(products)
        self.assertEqual(len(catalog),len(category_list))
        for category in category_list:
            for product in category:
                self.assertIn('https://krepkoshop.com/',product['url'])


if __name__ == '__main__':
    unittest.main()