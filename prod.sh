# !/bin/bash

ssh -o StrictHostKeyChecking=no ec2-user@$PROD_DEPLOY_SERVER
sudo docker stop $(sudo docker ps -a -q) && sudo docker rm $(sudo docker ps -a -q) && sudo docker run --name krepko-postgres -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -d -p 5432:5432 -v ~/docker/volumes/postgres:/var/lib/postgresql/data postgres && sudo docker run --name krepko-app -d -e TOKEN=$TELEGRAM_BOT_TOKEN -e CHAT_ID=$CHAT_ID -e DATABASE_URL=$DATABASE_URL romalukinskiy/krepko:latest